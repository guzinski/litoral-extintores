<?php

namespace LitoralExtintores\LojaBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;


/**
 * Description of ServicoImagem
 * @ORM\Table(name="servico_imagem", indexes={@ORM\Index(name="FK__servico", columns={"servico"})})
 * @ORM\Entity
 * @author Luciano
 */
class ServicoImagem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, fetch="LAZY")
     */
    private $media;

    /**
     * @var Servico
     *
     * @ORM\ManyToOne(targetEntity="Servico", inversedBy="imagens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="servico", referencedColumnName="id")
     * })
     */
    private $servico;
    
    
    public function __construct(Media $media = null, Servico $servico = null)
    {
        $this->media = $media;
        $this->servico = $servico;
    }
    
    public function getId()
    {
        return $this->id;
    }

        
    public function getMedia()
    {
        return $this->media;
    }

    public function getServico()
    {
        return $this->servico;
    }

    public function setMedia(Media $media)
    {
        $this->media = $media;
    }

    public function setServico(Servico $servico)
    {
        $this->servico = $servico;
    }


}
