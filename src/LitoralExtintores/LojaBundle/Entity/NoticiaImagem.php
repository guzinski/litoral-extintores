<?php

namespace LitoralExtintores\LojaBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProdutoImagem
 *
 * @ORM\Table(name="noticia_imagem", indexes={@ORM\Index(name="FK__noticia", columns={"noticia"})})
 * @ORM\Entity
 */
class NoticiaImagem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, fetch="LAZY")
     */
    private $media;

    /**
     * @var Noticia
     *
     * @ORM\ManyToOne(targetEntity="Noticia", inversedBy="imagens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia", referencedColumnName="id")
     * })
     */
    private $noticia;
    

    public function __construct(Media $media, Noticia $noticia)
    {
        $this->media = $media;
        $this->noticia = $noticia;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getNoticia()
    {
        return $this->noticia;
    }

    public function setMedia(Media $media)
    {
        $this->media = $media;
    }

    public function setNoticia(Noticia $noticia)
    {
        $this->noticia = $noticia;
    }
    
}
