<?php

namespace LitoralExtintores\LojaBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;


/**
 * Banner
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity
 */
class Banner
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=150, nullable=false)
     */
    private $nome;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text", nullable=true)
     */
    private $link;

    /**
     * @var float
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $ativo = 0;
    
    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, fetch="LAZY")
     */
    private $media;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    public function setMedia(Media $media)
    {
        $this->media = $media;
    }






}
