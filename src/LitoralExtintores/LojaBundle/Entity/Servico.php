<?php

namespace LitoralExtintores\LojaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Gedmo\Mapping\Annotation\Slug;



/**
 * Description of Servico
 * @ORM\Table(name="servico")
 * @ORM\Entity
 * @author Luciano
 */
class Servico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=false)
     */
    private $descricao;
    
    /**
     * @Slug(fields={"titulo"})
     * @ORM\Column(length=150, unique=true)
     */
    private $slug;

    
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $ativo = 0;


    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="ServicoImagem", mappedBy="servico", cascade={"all"})
     **/
    private $imagens;

    
    public function __construct()
    {
        $this->setImagens(new ArrayCollection());
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function getImagens()
    {
        return $this->imagens;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    public function setImagens(Collection $imagens)
    {
        $this->imagens = $imagens;
    }



    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


    
}
