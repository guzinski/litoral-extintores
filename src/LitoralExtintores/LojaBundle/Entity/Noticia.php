<?php

namespace LitoralExtintores\LojaBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Produto
 *
 * @ORM\Table(name="noticia")
 * @ORM\Entity
 */
class Noticia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descricao;

    
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $ativo = 0;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $dataCadastro;

    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="NoticiaImagem", mappedBy="noticia", cascade={"all"})
     **/
    private $imagens;
    
    public function __construct()
    {
        $this->setImagens(new ArrayCollection());
        $this->setDataCadastro(new DateTime("now"));
    }    

    public function getId()
    {
        return $this->id;
    }

        
    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function getImagens()
    {
        return $this->imagens;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    public function setImagens(Collection $imagens)
    {
        $this->imagens = $imagens;
    }


    
    function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    function setDataCadastro(DateTime $dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }


}
