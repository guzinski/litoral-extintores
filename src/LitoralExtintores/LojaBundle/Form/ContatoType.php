<?php

namespace LitoralExtintores\LojaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of ContatoType
 *
 * @author Luciano
 */
class ContatoType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome', 'text');
        $builder->add('email', 'email');
        $builder->add('mensagem', 'textarea');
    }

    public function getName()
    {
        return 'contato';
    }
    
}
