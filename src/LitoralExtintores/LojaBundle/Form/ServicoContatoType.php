<?php

namespace LitoralExtintores\LojaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of ServicoContatoForm
 *
 * @author Luciano
 */
class ServicoContatoType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome', 'text');
        $builder->add('email', 'email');
    }

    public function getName()
    {
        return 'servico';
    }

    
    
    
    
    
    
}
