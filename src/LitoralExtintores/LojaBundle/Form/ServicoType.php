<?php

namespace LitoralExtintores\LojaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ServicoType
 *
 * @author Luciano
 */
class ServicoType extends AbstractType
{
    
    public function getName()
    {
        return 'servico';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo');
        $builder->add('descricao', 'textarea', array("label"=> "Descrição"));
        $builder->add('ativo', 'choice', array(
            'choices' => array('1' => 'Sim', '0' => 'Não'),
            'expanded' => true,
            'label' => "Ativo",
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) 
    {
        $resolver->setDefaults(array(
                        'data_class' => 'LitoralExtintores\LojaBundle\Entity\Servico',
                    ));
    }
    
    
}
