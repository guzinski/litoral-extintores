<?php

namespace LitoralExtintores\LojaBundle\Controller\Loja;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Description of NoticiaController
 *
 * @author Luciano
 */
class NoticiaController extends Controller
{
    
    /**
     * @Route("/noticias/lista", name="_loja_noticia_lista")
     * @Template()
     * @return array
     */
    public function listaAction()
    {
        $noticias = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Noticia")->findBy(array("ativo"=>1), array("dataCadastro"=> "DESC"));
        
        return ["noticias"=>$noticias];
    }


    /**
     * @Route("/noticias/{id}", name="_loja_noticia")
     * @Template()
     * @param type $id
     * @return type
     * @throws NotFoundHttpException
     */
    public function indexAction($id=null)
    {
        if ($id>0) {
            $noticia = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Noticia")->findOneBy(array("id"=>$id, "ativo"=>1));
            if (null == $noticia) {
                throw new NotFoundHttpException;
            }
            return array("noticia"=>$noticia);
        } else {
            throw new NotFoundHttpException;
        }
        
    }
    

    
}

