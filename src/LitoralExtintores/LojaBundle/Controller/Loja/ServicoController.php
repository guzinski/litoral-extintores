<?php

namespace LitoralExtintores\LojaBundle\Controller\Loja;

use LitoralExtintores\LojaBundle\Form\ServicoContatoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Description of ServicoController
 *
 * @author Luciano
 */
class ServicoController extends Controller
{
    /**
     * 
     * @param type $id
     * @param type $page
     * @param type $order
     * @return type
     * @Route("/servicos/{slug}", name="loja_servico", requirements={"page": "\d+"})
     * @Template()
     */
    public function indexAction($slug, Request $request)
    {
        $repServico = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Servico");
        $servicos = $repServico->findAll();

        if (is_null($slug)) {
            throw new NotFoundHttpException;
        } else {
            $servico = $repServico->findOneBy(["slug"=>$slug]);
        }
        if (empty($servico)) {
            throw new NotFoundHttpException;
        }
        
        $form = $this->createForm(new ServicoContatoType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = Swift_Message::newInstance()
                ->setSubject('Novo de Interesse em serviço '.$servico->getTitulo())
                ->setFrom('naoresponda@litoralextintoresloja.com.br')
                ->addReplyTo($form->get("email")->getData(), $form->get("nome")->getData())
                ->setTo('litoralextintores1@hotmail.com')
                ->addBcc("comercial@agenciawi9.com.br")
                ->setBody(
                    $this->renderView(
                        'LitoralExtintoresLojaBundle::Loja\\Emails\\servico.html.twig',
                        array(
                                'servico'   => $servico,
                                'nome'      => $form->get("nome")->getData(),
                                'email'     => $form->get("email")->getData(),
                        )
                    ),
                    "text/html"
                );
                
            $this->get('mailer')->send($message);
            
            $this->redirectToRoute("loja_servico", array("slug"=>$slug));
        } 

        
        
        
        
        return array(
            "servicos" =>  $servicos, 
            "servicoAtual" =>  $servico, 
            "form" => $form->createView()
        );
    }
}
