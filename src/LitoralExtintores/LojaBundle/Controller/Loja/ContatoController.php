<?php

namespace LitoralExtintores\LojaBundle\Controller\Loja;

use LitoralExtintores\LojaBundle\Form\ContatoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ContatoController
 *
 * @author Luciano
 */
class ContatoController extends Controller
{
    /**
     * @Route("/litoralExtintores/contato", name="_loja_contato")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new ContatoType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = Swift_Message::newInstance()
                ->setSubject('Novo de Contato '.$form->get("nome")->getData().' pelo site Litoral Extintores')
                ->setFrom('naoresponda@litoralextintoresloja.com.br')
                ->addReplyTo($form->get("email")->getData(), $form->get("nome")->getData())
                ->setTo('litoralextintores1@hotmail.com')
                ->addBcc("comercial@agenciawi9.com.br")
                ->setBody(
                    $this->renderView(
                        'LitoralExtintoresLojaBundle::Loja\\Emails\\contato.html.twig',
                        array(
                                'nome'      => $form->get("nome")->getData(),
                                'email'     => $form->get("email")->getData(),
                                'mensagem'  => $form->get("mensagem")->getData(),
                        )
                    ),
                    "text/html"
                );
                
            $this->get('mailer')->send($message);

            $this->redirectToRoute("_loja_contato");
        }
        
        return array('form' => $form->createView());

    }
}
