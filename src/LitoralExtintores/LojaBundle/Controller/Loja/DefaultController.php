<?php

namespace LitoralExtintores\LojaBundle\Controller\Loja;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    
    /**
     * @Route("/", name="_loja_index")
     * @Template()
     */
    public function indexAction()
    {
        $banners = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Banner")->findBy(array("ativo"=>"1"));
        $produtos = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Produto")->getProdutosDestaques();
        $descontos = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Produto")->getProdutosComDesconto();
        $noticias = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Noticia")->findBy(['ativo'=>1], ["dataCadastro"=>"desc"], 3);
        
        return array("banners"=>$banners, "produtos"=>$produtos, "descontos"=>$descontos, "noticias"=>$noticias);
    }
    
    /**
     * 
     * @return type
     */
    public function renderMenuAction()
    {
        $categorias = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Categoria")->findBy([], ['nome' => 'ASC']);
        $servicos = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Servico")->findAll();
        
        return $this->render(
            'LitoralExtintoresLojaBundle::Loja\\menu.html.twig',
            array('categorias' => $categorias, "servicos"=>$servicos)
        );
    }
}
