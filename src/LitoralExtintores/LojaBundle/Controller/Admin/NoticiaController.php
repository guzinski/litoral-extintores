<?php

namespace LitoralExtintores\LojaBundle\Controller\Admin;

use LitoralExtintores\LojaBundle\Entity\Noticia;
use LitoralExtintores\LojaBundle\Entity\NoticiaImagem;
use LitoralExtintores\LojaBundle\Form\NoticiaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of NoticiaController
 *
 * @Route("/noticia")
 * @author Luciano
 */
class NoticiaController extends Controller
{

    /**
     * @Route("/", name="_noticia")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/pagination", name="_noticia_pagination")
     * @return Response
     */
    public function paginationAction()
    {
        $noticias = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Noticia")->findAll();
        $dados = array();
        foreach ($noticias as $noticia) {
            $dados[] = [
                "<a href=\"".$this->generateUrl("_noticia_form", array("id"=>$noticia->getId())) ."\">". $noticia->getTitulo() ."</a>",
                $noticia->getAtivo() ? "Sim" : "Não",
                "<a href=\"javascript:excluirNoticia(".$noticia->getId() .");\"><i class=\"glyphicon glyphicon-trash\"></a>",
            ];
        }
        $return['recordsTotal'] = count($noticias);
        $return['recordsFiltered'] = count($noticias);
        $return['data'] = $dados;
        return new Response(json_encode($return));
    }
    
    

    /**
     * 
     * @Route("/editar/{id}", name="_noticia_form")
     * @Template()
     */
    public function formAction(Request $request, $id = 0) 
    {
        $mm = $this->container->get('sonata.media.manager.media');
        $em = $this->getDoctrine()->getManager();
        
        if ($id>0) {
            $noticia = $em->find("LitoralExtintoresLojaBundle:Noticia", $id);
        } else {
            $noticia = new Noticia();
        }
        
        $form = $this->createForm(new NoticiaType(), $noticia);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $excluidas = $request->get("excluidas", array());
            $imagens = $request->get("imagens", array());
            foreach ($excluidas as $media) {
                $objImage = $em->find("LitoralExtintoresLojaBundle:NOticiaImagem", $media);
                $noticia->getImagens()->removeElement($objImage);
                
                $em->remove($objImage);
            }
            foreach ($imagens as $imagem) {
                $noticia->getImagens()->add(new NoticiaImagem($mm->find($imagem), $noticia));
            }
            $em->persist($noticia);
            $em->flush();
            return $this->redirectToRoute("_noticia");
        }
        
        return array("noticia"=>$noticia, "form"=>$form->createView());
    }

    /**
     * @Route("/excluir", name="_noticia_excluir")
     */
    public function excluiProdutoAction(Request $resquest) 
    {
        $respone = array();
        $id = $resquest->request->getInt("id", null);
        if (null != $id) {
            $em = $this->getDoctrine()->getManager();
            $noticia = $em->find("LitoralExtintoresLojaBundle:Noticia", $id);
            $em->remove($noticia);
            $em->flush();
            $respone['ok'] = 1;
        } else {
            $respone['ok'] = 0;
            $respone['error'] = "Erro ao exclui notícia";
        }
        return new Response(json_encode($respone));
    }


    
    
}
