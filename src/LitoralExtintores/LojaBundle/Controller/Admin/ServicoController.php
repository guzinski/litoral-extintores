<?php

namespace LitoralExtintores\LojaBundle\Controller\Admin;

use LitoralExtintores\LojaBundle\Entity\Servico;
use LitoralExtintores\LojaBundle\Entity\ServicoImagem;
use LitoralExtintores\LojaBundle\Form\ServicoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of ServicoController
 *
 * @Route("/servico")
 * @author Luciano
 */
class ServicoController extends Controller
{
    /**
     * @Route("/", name="servico")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/pagination", name="servico_pagination")
     * @return Response
     */
    public function paginationAction()
    {
        $servicos = $this->getDoctrine()->getRepository("LitoralExtintoresLojaBundle:Servico")->findAll();
        $dados = array();
        foreach ($servicos as $servicos) {
            $dados[] = [
                "<a href=\"".$this->generateUrl("servico_form", array("id"=>$servicos->getId())) ."\">". $servicos->getTitulo() ."</a>",
                $servicos->getAtivo() ? "Sim" : "Não",
                "<a href=\"javascript:excluirServico(".$servicos->getId() .");\"><i class=\"glyphicon glyphicon-trash\"></a>",
            ];
        }
        $return['recordsTotal'] = count($servicos);
        $return['recordsFiltered'] = count($servicos);
        $return['data'] = $dados;
        return new Response(json_encode($return));
    }
    
    
    /**
     * 
     * @Route("/editar/{id}", name="servico_form")
     * @Template()
     */
    public function formAction(Request $request, $id = 0) 
    {
        $mm = $this->container->get('sonata.media.manager.media');
        $em = $this->getDoctrine()->getManager();
        
        if ($id>0) {
            $servico = $em->find("LitoralExtintoresLojaBundle:Servico", $id);
        } else {
            $servico = new Servico();
        }
        
        $form = $this->createForm(new ServicoType(), $servico);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $excluidas = $request->get("excluidas", array());
            $imagens = $request->get("imagens", array());
            foreach ($excluidas as $media) {
                $objImage = $em->find("LitoralExtintoresLojaBundle:ServicoImagem", $media);
                $servico->getImagens()->removeElement($objImage);
                
                $em->remove($objImage);
            }
            foreach ($imagens as $imagem) {
                $servico->getImagens()->add(new ServicoImagem($mm->find($imagem), $servico));
            }
            $em->persist($servico);
            $em->flush();
            return $this->redirectToRoute("servico");
        }
        
        return array("servico"=>$servico, "form"=>$form->createView());
    }

    /**
     * @Route("/excluir", name="servico_excluir")
     */
    public function excluiServicoAction(Request $resquest) 
    {
        $respone = array();
        $id = $resquest->request->getInt("id", null);
        if (null != $id) {
            $em = $this->getDoctrine()->getManager();
            $servico = $em->find("LitoralExtintoresLojaBundle:Servico", $id);
            $em->remove($servico);
            $em->flush();
            $respone['ok'] = 1;
        } else {
            $respone['ok'] = 0;
            $respone['error'] = "Erro ao exclui serviço";
        }
        return new Response(json_encode($respone));
    }

    
    
}
